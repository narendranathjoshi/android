package co.nnjoshi.android.inquest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        LinearLayout aboutLinearLayout = (LinearLayout) findViewById(R.id.linearLayoutAbout);
        aboutLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
                alertDialogBuilder.setMessage(Html.fromHtml(
                        "InQuest is an app which let's you search the breadth and depth of your Android device. <br><br>" +
                                "You can search anything like apps, contacts, files, documents, images and videos! <br><br>" +
                                "User privacy and comfort is of utmost importance. No data leaves your phone. Ever.<br><br><br>" +
                                " - nnjoshi.co (<a href='mailto:android@nnjoshi.co'>android@nnjoshi.co</a>)"));

                alertDialogBuilder.setNegativeButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        LinearLayout permissionsLinearLayout = (LinearLayout) findViewById(R.id.linearLayoutPermissions);
        permissionsLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
                alertDialogBuilder.setMessage(Html.fromHtml(
                        "STORAGE - Required to index the files, images, videos, music and documents for fast and efficient search <br><br>" +
                        "CONTACTS - Required to index contacts for fast and efficient search <br><br>" +
                        "INTERNET - Required to check for regular updates"
                ));

                alertDialogBuilder.setNegativeButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        LinearLayout syncLinearLayout = (LinearLayout) findViewById(R.id.linearLayoutSync);
        syncLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InquestApplication.sync(SettingsActivity.this, true);
            }
        });

        ImageView headerImageView = (ImageView) findViewById(R.id.headerImageView);
        headerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
