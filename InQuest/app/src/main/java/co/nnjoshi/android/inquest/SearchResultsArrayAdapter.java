package co.nnjoshi.android.inquest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import co.nnjoshi.android.inquest.database.DatabaseEntry;
import co.nnjoshi.android.inquest.utils.ResultType;

/**
 * Created by narendranathjoshi on 02/03/16.
 */
public class SearchResultsArrayAdapter extends ArrayAdapter<DatabaseEntry> {
    private final Context context;
    private final List<DatabaseEntry> values;
    private final ImageLoader imageLoader;

    public SearchResultsArrayAdapter(Context context, List<DatabaseEntry> values, ImageLoader imageLoader) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.imageLoader = imageLoader;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.results_list_item, parent, false);

        TextView packageNameTextView = (TextView) rowView.findViewById(R.id.list_item_package_name);
        TextView titleTextView = (TextView) rowView.findViewById(R.id.list_item_title);
        TextView typeTextView = (TextView) rowView.findViewById(R.id.list_item_type);
        ImageView iconView = (ImageView) rowView.findViewById(R.id.list_item_icon);

        packageNameTextView.setText(values.get(position).getPackageName());
        titleTextView.setText(values.get(position).getTitle());
        typeTextView.setText(values.get(position).getType());

        String type = typeTextView.getText().toString();
        String packageName = packageNameTextView.getText().toString();
        String title = titleTextView.getText().toString();

        if (type.equalsIgnoreCase(ResultType.App.name())) {
            try{
                Drawable icon = context.getPackageManager().getApplicationIcon(packageName);
                iconView.setImageDrawable(icon);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace(); // TODO get fallback app icon here and set to imageView
            }
        } else if (type.equalsIgnoreCase(ResultType.Contact.name())) {
            iconView.setImageBitmap(InquestApplication.retrieveContactPhoto(context, packageName));
        } else if (type.equalsIgnoreCase(ResultType.Video.name())) {
//            File videoFile = new File(packageName);
//            Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(videoFile.getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
            imageLoader.displayImage("file://" + packageName, iconView);
        } else if (type.equalsIgnoreCase(ResultType.Image.name())) {
            imageLoader.displayImage("file://" + packageName, iconView);
        } else if (type.equalsIgnoreCase(ResultType.File.name())) {
            if (InquestApplication.getFileExtension(title).equalsIgnoreCase("pdf")) {
                iconView.setImageDrawable(context.getResources().getDrawable(R.drawable.pdf));
            } else if (InquestApplication.getFileExtension(title).equalsIgnoreCase("doc") || InquestApplication.getFileExtension(title).equalsIgnoreCase("docx")) {
                iconView.setImageDrawable(context.getResources().getDrawable(R.drawable.doc));
            } else if (InquestApplication.getFileExtension(title).equalsIgnoreCase("txt")) {
                iconView.setImageDrawable(context.getResources().getDrawable(R.drawable.txt));
            } else if (InquestApplication.getFileExtension(title).equalsIgnoreCase("apk")) {
                iconView.setImageDrawable(context.getResources().getDrawable(R.drawable.apk));
            }
        } else if (type.equalsIgnoreCase(ResultType.Audio.name())) {
            iconView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_audiotrack_black_48dp));
        }

        return rowView;
    }

}
