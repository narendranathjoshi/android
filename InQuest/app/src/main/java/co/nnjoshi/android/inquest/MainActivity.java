package co.nnjoshi.android.inquest;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avocarrot.androidsdk.AvocarrotInstream;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;

import co.nnjoshi.android.inquest.database.DatabaseEntry;
import co.nnjoshi.android.inquest.database.DatabaseHandler;
import co.nnjoshi.android.inquest.utils.AppSharedPreferences;
import co.nnjoshi.android.inquest.utils.ResultType;

public class MainActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;  // random unique number
    DatabaseHandler dbHandler;
    private Tracker mTracker;
    private ListView searchResultListView;
    private ImageLoader imageLoader;

    private final int TRIGGER_SEARCH = 1;
    private final long SEARCH_TRIGGER_DELAY_IN_MS = 300;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == TRIGGER_SEARCH) {
                String s = msg.getData().getString("s");
                Log.d("SUBMISSION THROTTLING", "called search for " + s);
                sendSearch(s);

                // tracker for Google Analytics
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("Search")
                        .setLabel(s)
                        .build());
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        requestSyncPermissions();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        requestSyncPermissions();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the shared Tracker instance.
        InquestApplication application = (InquestApplication) getApplication();
        mTracker = application.getDefaultTracker();

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true).cacheOnDisk(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);
        imageLoader = ImageLoader.getInstance();

        SharedPreferences.Editor editor = AppSharedPreferences.editor(getApplicationContext());

        dbHandler = new DatabaseHandler(getApplicationContext());
        setContentView(R.layout.activity_main);

        boolean isWidgetEnabled = getIntent().getBooleanExtra(InquestApplication.SHAREDPREFS_WIDGET, false);
        Log.d(InquestApplication.SHAREDPREFS_WIDGET, String.valueOf(getIntent().getBooleanExtra(InquestApplication.SHAREDPREFS_WIDGET, false)));

        isWidgetEnabled = isWidgetEnabled || AppSharedPreferences.getSharedPreferences(getApplicationContext()).getBoolean(InquestApplication.SHAREDPREFS_WIDGET, false);

        editor.putBoolean(InquestApplication.SHAREDPREFS_WIDGET, isWidgetEnabled);
        editor.commit();

        if (!isWidgetEnabled && !hasIrritatedUserEnoughForWidget()) {
            requestWidgetWithDialog();
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            InquestApplication.setContactsPermission(getApplicationContext(), true);
            InquestApplication.setStoragePermission(getApplicationContext(), true);
        } else {
            requestSyncPermissions();
        }


        boolean isSync = AppSharedPreferences.getSharedPreferences(getApplicationContext()).getBoolean(InquestApplication.SHAREDPREFS_SYNC, false);

        if (!isSync) InquestApplication.sync(this, true);

        boolean alarmUp = (PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(getApplicationContext(), IndexService.class), PendingIntent.FLAG_NO_CREATE) != null);

        if (!alarmUp) {
            Calendar cal = Calendar.getInstance();
            Intent intent = new Intent(MainActivity.this, IndexService.class);
            PendingIntent pintent = PendingIntent.getService(MainActivity.this, 0, intent, 0);
            AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            // schedule for 4 hours
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 4 * 60 * 60 * 1000, pintent);
        }

        ImageView settingsImageView = (ImageView) findViewById(R.id.imageViewSettings);
        settingsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            }
        });

        searchResultListView = (ListView) findViewById(R.id.listViewResultList);

        SearchResultsArrayAdapter adapter = new SearchResultsArrayAdapter(getApplicationContext(), new ArrayList<DatabaseEntry>(), imageLoader);
        adapter.add(new DatabaseEntry("Apps, Contacts, Files, Media", "Search Anything Here!", "Go on!", null, null));
        searchResultListView.setAdapter(adapter);

        final EditText editTextSearchField = (EditText) findViewById(R.id.editTextSearchString);
        editTextSearchField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearchField.getText().clear();
            }
        });

        editTextSearchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                handler.removeMessages(TRIGGER_SEARCH);
                Message msg = new Message();
                msg.what = TRIGGER_SEARCH;
                Bundle data = new Bundle();
                data.putString("s", s.toString());
                msg.setData(data);
                handler.sendMessageDelayed(msg, SEARCH_TRIGGER_DELAY_IN_MS);
            }
        });
    }

    private void requestSyncPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InquestApplication.setContactsPermission(getApplicationContext(), ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);

            InquestApplication.setStoragePermission(getApplicationContext(), ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

            Log.d("Contacts Permission", String.valueOf(InquestApplication.getContactsPermission(getApplicationContext())));
            Log.d("Storage Permission", String.valueOf(InquestApplication.getStoragePermission(getApplicationContext())));

            if (!InquestApplication.getContactsPermission(getApplicationContext()) && !InquestApplication.getStoragePermission(getApplicationContext())) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            } else if (InquestApplication.getContactsPermission(getApplicationContext()) && !InquestApplication.getStoragePermission(getApplicationContext())) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            } else if (!InquestApplication.getContactsPermission(getApplicationContext()) && InquestApplication.getStoragePermission(getApplicationContext())) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }

    private void sendSearch(CharSequence s) {
        if (s.toString().length() < 1) {
            final SearchResultsArrayAdapter adapter = new SearchResultsArrayAdapter(getApplicationContext(), new ArrayList<DatabaseEntry>(), imageLoader);
            adapter.add(new DatabaseEntry("Apps, Contacts, Files, Media", "Search Anything Here!", "Go on!", null, null));
            searchResultListView.setAdapter(adapter);
            return;
        }

        try {
            List<DatabaseEntry> results = dbHandler.searchEntry(s.toString().trim());
            Collections.sort(results, new Comparator<DatabaseEntry>() {
                @Override
                public int compare(DatabaseEntry entry1, DatabaseEntry entry2) {
                    return entry2.getScore() - entry1.getScore();
                }
            });

            final SearchResultsArrayAdapter adapter = new SearchResultsArrayAdapter(getApplicationContext(), results, imageLoader);

            if (InquestApplication.isPro(getApplicationContext())) { // pro users don't see ads
                searchResultListView.setAdapter(adapter);
            } else {
                // Create the AvocarrotAdapter that encapsulates your adapter
                AvocarrotInstream avocarrotInstream = new AvocarrotInstream(
                        adapter,        /* pass your listAdapter */
                        this,           /* reference to your Activity */
                        "a0baa6ecebe93b1b953cb1137649e6c24e22467b", /* this is your Avocarrot API Key */
                        "09d5500fdeab424d8a31cea6ed9296a1ff7c7024"  /* this is your Avocarrot Placement Key */
                );
                avocarrotInstream.setLogger(true, "ALL");
                avocarrotInstream.setSandbox(false);
                avocarrotInstream.setFrequency(4, 5);

                // Bind the created avocarrotInstream adapter to your list instead of your listAdapter
                searchResultListView.setAdapter(avocarrotInstream);

                Log.d("Number of results", String.valueOf(results.size()));
            }
        } catch (SQLiteDatabaseLockedException e) {
            Log.d("DB locked exception", "");
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("SQLiteDatabaseLockedException")
                    .build());
        }

        searchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String title = ((TextView) ((LinearLayout) ((LinearLayout) view).getChildAt(0)).getChildAt(0)).getText().toString();
                String packageName = ((TextView) ((LinearLayout) ((LinearLayout) ((LinearLayout) view).getChildAt(1)).getChildAt(1)).getChildAt(0)).getText().toString();
                String type = ((TextView) ((LinearLayout) ((LinearLayout) ((LinearLayout) view).getChildAt(1)).getChildAt(1)).getChildAt(1)).getText().toString();

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("Result Item Click")
                        .setLabel(String.format("%s (%s) - %s", title, packageName, type))
                        .build());

                if (type.equalsIgnoreCase(ResultType.App.name())) {
                    try {
                        startActivity(getApplicationContext().getPackageManager().getLaunchIntentForPackage(packageName));
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Cannot open app " + title, Toast.LENGTH_SHORT).show();
                    }
                } else if (type.equalsIgnoreCase(ResultType.Contact.name())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + packageName));
                    startActivity(intent);
                } else if (type.equalsIgnoreCase(ResultType.Audio.name()) ||
                        type.equalsIgnoreCase(ResultType.File.name()) ||
                        type.equalsIgnoreCase(ResultType.Image.name()) ||
                        type.equalsIgnoreCase(ResultType.Video.name())) {
                    File thisFile = new File(packageName);
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    Intent newIntent = new Intent(Intent.ACTION_VIEW);
                    String extension = InquestApplication.getFileExtension(packageName);
                    Log.d("FILE EXTENSION for " + packageName, thisFile.getName() + ":" + extension);
                    String mimeType = myMime.getMimeTypeFromExtension(extension);
                    newIntent.setDataAndType(Uri.fromFile(thisFile), mimeType);
                    newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        getApplicationContext().startActivity(newIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(getApplicationContext(), "Could not open file.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                for( int i = 0; i < permissions.length; i++ ) {
                    if( grantResults[i] == PackageManager.PERMISSION_GRANTED ) {
                        if (permissions[i].equals(Manifest.permission.READ_CONTACTS)) InquestApplication.setContactsPermission(getApplicationContext(), true);
                        else InquestApplication.setStoragePermission(getApplicationContext(), true);
                    } else if( grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                        if (permissions[i].equals(Manifest.permission.READ_CONTACTS)) InquestApplication.setContactsPermission(getApplicationContext(), false);
                        else InquestApplication.setStoragePermission(getApplicationContext(), false);
                    }
                }
            }
        }
        InquestApplication.sync(MainActivity.this, true);
    }

    void requestWidgetWithDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Welcome!\nFor best experience and fast access, " +
                "please consider adding InQuest's widget on your home screen");

        final SharedPreferences.Editor editor = AppSharedPreferences.editor(getApplicationContext());

                alertDialogBuilder.setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putInt(InquestApplication.SHAREDPREFS_WIDGET_REQUEST, 4);
                editor.commit();

                Log.d(InquestApplication.SHAREDPREFS_WIDGET_REQUEST, String.valueOf(AppSharedPreferences.getSharedPreferences(getApplicationContext()).getInt("widget_request", 0)));
            }
        });

        alertDialogBuilder.setPositiveButton("I will Do It!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putInt(InquestApplication.SHAREDPREFS_WIDGET_REQUEST,
                        AppSharedPreferences.getSharedPreferences(getApplicationContext())
                                .getInt(InquestApplication.SHAREDPREFS_WIDGET_REQUEST, 0) + 1);
                editor.commit();

                Log.d(InquestApplication.SHAREDPREFS_WIDGET_REQUEST, String.valueOf(AppSharedPreferences.getSharedPreferences(getApplicationContext()).getInt("widget_request", 0)));
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    
    boolean hasIrritatedUserEnoughForWidget() {
        return AppSharedPreferences.getSharedPreferences(
                getApplicationContext()).getInt(InquestApplication.SHAREDPREFS_WIDGET_REQUEST, 0) > 3;
    }
}