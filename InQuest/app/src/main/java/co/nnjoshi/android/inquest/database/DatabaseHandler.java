package co.nnjoshi.android.inquest.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.nnjoshi.android.inquest.InquestApplication;

/**
 * Created by narendranathjoshi on 08/04/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 10;

    // Database name
    private static final String DATABASE_NAME = "inquestEntries";

    // Contacts table name
    private static final String TABLE_ENTRIES = "entries";
    private static final String FTS_ENTRIES = "fts_entries";

    // Contacts Table Columns names
    private static final String KEY_PACKAGE_NAME = "package_name";
    private static final String KEY_TITLE = "title";
    private static final String KEY_TYPE = "type";
    private static final String KEY_IMAGE_URI = "image_uri";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_SCORE = "score";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ENTRIES_TABLE = String.format(
                "CREATE TABLE IF NOT EXISTS %s " +
                        "(id INTEGER PRIMARY KEY, %s TEXT NULL, %s TEXT NULL, %s TEXT NULL, %s TEXT NULL, %s TEXT NULL, %s INTEGER NULL)",
                TABLE_ENTRIES, KEY_PACKAGE_NAME, KEY_TITLE, KEY_TYPE, KEY_IMAGE_URI, KEY_PHONE, KEY_SCORE);
        db.execSQL(CREATE_ENTRIES_TABLE);

        String ftsCreateQuery = String.format("CREATE VIRTUAL TABLE %s USING fts4" +
                "(content='%s', %s, %s)",
                FTS_ENTRIES, TABLE_ENTRIES, KEY_PACKAGE_NAME, KEY_TITLE);
        db.execSQL(ftsCreateQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        Log.d("Database", "Database Upgraded");
        db.execSQL(String.format("DROP TABLE IF EXISTS %s", TABLE_ENTRIES));
        db.execSQL(String.format("DROP TABLE IF EXISTS %s", FTS_ENTRIES));

        // Create tables again
        onCreate(db);
        db.close();
    }

    /**
     * Adding multiple entries
     */
    public void addEntries(List<DatabaseEntry> entries)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            db.beginTransaction();

            try {
                for (DatabaseEntry entry : entries) {
                    values.put(KEY_PACKAGE_NAME, entry.getPackageName());
                    values.put(KEY_TITLE, entry.getTitle());
                    values.put(KEY_TYPE, entry.getType());
                    values.put(KEY_IMAGE_URI, entry.getImageUri());
                    values.put(KEY_PHONE, entry.getPhone());

                    int score = 1;
                    if (entry.getPackageName().equalsIgnoreCase(entry.getTitle())) {
                        score = 0;
                    }
                    values.put(KEY_SCORE, score);

                    db.insert(TABLE_ENTRIES, null, values);
                }
                db.setTransactionSuccessful();
            } finally {
                String ftsInsertQuery = String.format("INSERT INTO %s (docid, %s, %s) SELECT id, %s, %s FROM %s",
                        FTS_ENTRIES, KEY_PACKAGE_NAME, KEY_TITLE, KEY_PACKAGE_NAME, KEY_TITLE, TABLE_ENTRIES);
                db.execSQL(ftsInsertQuery);
                db.endTransaction();
            }
        } catch (SQLiteDatabaseLockedException e) {
            // do nothing
        }
    }

    /**
     * Searching for entries by query
     */
    public List<DatabaseEntry> searchEntry(String query) {
        List<DatabaseEntry> entries = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s IN (SELECT %s FROM %s WHERE %s MATCH ?) ORDER BY %s DESC",
                TABLE_ENTRIES, KEY_PACKAGE_NAME, KEY_PACKAGE_NAME, FTS_ENTRIES, FTS_ENTRIES, KEY_SCORE);
        String[] selectionArgs = { query + "*" };
        Cursor cursor = db.rawQuery(sql, selectionArgs);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseEntry entry = new DatabaseEntry();
                entry.setPackageName(cursor.getString(1));
                entry.setTitle(cursor.getString(2));
                entry.setType(cursor.getString(3));
                entry.setImageUri(cursor.getString(4));
                entry.setPhone(cursor.getString(5));
                entry.setScore(InquestApplication.scoreResult(query, entry.getTitle(), entry.getPackageName(), entry.getPhone()));
                // Adding entry to list
                entries.add(entry);
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        // return contact list
//        int endIndex = (entries.size() < 50) ?  entries.size(): 50;
        int endIndex = entries.size();
        return entries.subList(0, endIndex);
    }


    public void vacuumDatabase() {
        Log.d("Database", "FTS Vacuum Initialized");
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(String.format("DROP TABLE IF EXISTS %s", TABLE_ENTRIES));
        db.execSQL(String.format("DROP TABLE IF EXISTS %s", FTS_ENTRIES));

        // Create tables again
        onCreate(db);
        db.close();
    }
}
