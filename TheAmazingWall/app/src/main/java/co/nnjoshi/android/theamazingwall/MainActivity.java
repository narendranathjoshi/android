package co.nnjoshi.android.theamazingwall;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import co.nnjoshi.android.theamazingwall.utils.ImageAPITask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout changeNowLinearLayout =  (LinearLayout) findViewById(R.id.linearLayoutChangeNow);
        changeNowLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImageAPITask(MainActivity.this).execute();
            }
        });

        LinearLayout changeDailyLinearLayout =  (LinearLayout) findViewById(R.id.linearLayoutDaily);
        changeDailyLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
