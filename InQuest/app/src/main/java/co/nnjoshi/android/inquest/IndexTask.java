package co.nnjoshi.android.inquest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import co.nnjoshi.android.inquest.database.DatabaseEntry;
import co.nnjoshi.android.inquest.database.DatabaseHandler;
import co.nnjoshi.android.inquest.utils.AppSharedPreferences;
import co.nnjoshi.android.inquest.utils.ResultType;


/**
 * Created by narendranathjoshi on 06/03/16.
 */
public class IndexTask extends AsyncTask {
    private final Context context;
    private DatabaseHandler dbHandler;
    private ArrayList<DatabaseEntry> fileEntries;
    private ArrayList<DatabaseEntry> audioEntries;
    private ArrayList<DatabaseEntry> videoEntries;
    private ArrayList<DatabaseEntry> imageEntries;
    private boolean showProgress;
    private ProgressDialog syncDialog;

    public IndexTask(Context context, boolean showProgress) {
        this.context = context;
        this.dbHandler = new DatabaseHandler(context);
        this.showProgress = showProgress;
    }

    private void getInstalledApps() {
        final PackageManager pm = context.getPackageManager();

        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        List<DatabaseEntry> appEntries = new ArrayList<>();
        for (ApplicationInfo packageInfo: packages) {
            appEntries.add(new DatabaseEntry(packageInfo.packageName, packageInfo.loadLabel(pm).toString(), ResultType.App.name(), null, null));
        }

        dbHandler.addEntries(appEntries);
    }


    void getContacts() {

        ArrayList<DatabaseEntry> contactEntries = new ArrayList<>();

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Cursor cursor = context.getContentResolver().query(uri, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,   ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME}, selection, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            contactEntries.add(new DatabaseEntry(contactNumber, contactName, ResultType.Contact.name(), null, contactNumber));

            cursor.moveToNext();
        }
        cursor.close();

        dbHandler.addEntries(contactEntries);
    }

    void getFiles() {
        Log.d("Files", "Starting ...");
        fileEntries = new ArrayList<>();
        audioEntries = new ArrayList<>();
        videoEntries = new ArrayList<>();
        imageEntries = new ArrayList<>();

        String path = Environment.getExternalStorageDirectory().getPath();
        File f = new File(path);
        walk(f);

        ArrayList<DatabaseEntry> entries = new ArrayList<>();
        entries.addAll(fileEntries);
        entries.addAll(audioEntries);
        entries.addAll(imageEntries);
        entries.addAll(videoEntries);

        dbHandler.addEntries(entries);
        Log.d("Files", "Done");
    }

    private void walk(File root) {
        File[] list = root.listFiles();

        for (File file : list) {
            if (file.isDirectory()) {
                walk(file);
            }
            else {
                if (InquestApplication.isFileExtensionAllowed(file.getName(), ResultType.File.name())) {
                    fileEntries.add(new DatabaseEntry(file.getPath(), file.getName(), ResultType.File.name(), null, null));
                } if (InquestApplication.isFileExtensionAllowed(file.getName(), ResultType.Audio.name())) {
                    audioEntries.add(new DatabaseEntry(file.getPath(), file.getName(), ResultType.Audio.name(), null, null));
                } else if (InquestApplication.isFileExtensionAllowed(file.getName(), ResultType.Image.name())) {
                    imageEntries.add(new DatabaseEntry(file.getPath(), file.getName(), ResultType.Image.name(), null, null));
                } else if (InquestApplication.isFileExtensionAllowed(file.getName(), ResultType.Video.name())) {
                    videoEntries.add(new DatabaseEntry(file.getPath(), file.getName(), ResultType.Video.name(), null, null));
                }
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) {
            syncDialog = new ProgressDialog(context);
            syncDialog.setMessage("Setting Up Things. Shouldn't take long!");
            //show dialog
            syncDialog.show();
            syncDialog.setCancelable(false);
            syncDialog.setCanceledOnTouchOutside(false);
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {
        Log.d("IndexTask", "Indexing database ...");
        dbHandler.vacuumDatabase();
        publishProgress("Woohoo!");
        getInstalledApps();
        publishProgress("Running fast!");
        if (InquestApplication.getContactsPermission(context)) {
            Log.d("IndexTask", "Indexing contacts ...");
            getContacts();
        }
        publishProgress("Yep, we're almost done!");
        if (InquestApplication.getStoragePermission(context)) {
            Log.d("IndexTask", "Indexing files ...");
            getFiles();
        }

        SharedPreferences.Editor editor = AppSharedPreferences.editor(context);
        editor.putBoolean(InquestApplication.SHAREDPREFS_SYNC, true);
        editor.commit();

        return null;
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        if (showProgress) syncDialog.setMessage(values[0].toString());
    }

    @Override
    protected void onPostExecute(Object o) {
        if (showProgress) syncDialog.dismiss();
    }
}
