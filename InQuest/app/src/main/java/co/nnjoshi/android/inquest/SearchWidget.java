package co.nnjoshi.android.inquest;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import co.nnjoshi.android.inquest.utils.AppSharedPreferences;

/**
 * Implementation of App Widget functionality.
 */
public class SearchWidget extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.search_widget);

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("widget", true);
        intent.setAction("com.random.action");

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        views.setOnClickPendingIntent(R.id.imageViewWidget, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetIds, views);
    }


    @Override
    public void onEnabled(Context context) {
        SharedPreferences.Editor editor = AppSharedPreferences.editor(context);
        editor.putBoolean("widget", true);
        editor.commit();
    }

    @Override
    public void onDisabled(Context context) {
        SharedPreferences.Editor editor = AppSharedPreferences.editor(context);
        editor.putBoolean("widget", false);
        editor.commit();
    }
}

