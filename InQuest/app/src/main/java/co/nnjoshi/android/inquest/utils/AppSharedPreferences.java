package co.nnjoshi.android.inquest.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by narendranathjoshi on 07/03/16.
 */
public class AppSharedPreferences {
    private static String inquestPreferences = "inquestPreferences";

    public static SharedPreferences.Editor editor(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(inquestPreferences, Context.MODE_PRIVATE);

        return sharedpreferences.edit();
    }

    public static SharedPreferences getSharedPreferences(Context context) {

        return context.getSharedPreferences(inquestPreferences, Context.MODE_PRIVATE);
    }
}
