package co.nnjoshi.android.inquest;

import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import co.nnjoshi.android.inquest.utils.AppSharedPreferences;
import co.nnjoshi.android.inquest.utils.ResultType;

/**
 * Created by narendranathjoshi on 10/04/16.
 */
public class InquestApplication extends Application {
    private Tracker mTracker;

    final static String[] ALLOWED_TEXT_EXTENSIONS = {"pdf", "doc", "txt", "apk"};
    final static String[] ALLOWED_AUDIO_EXTENSIONS = {"mp3", "aac", "ogg", "wav"};
    final static String[] ALLOWED_VIDEO_EXTENSIONS = {"3gp", "mp4", "mkv"};
    final static String[] ALLOWED_IMAGE_EXTENSIONS = {"jpeg", "jpg", "gif", "png", "bmp"};

    public static boolean CONTACTS_PERMISSION_GRANTED;
    public static boolean STORAGE_PERMISSION_GRANTED;

    public static final String SHAREDPREFS_PERMS_CONTACTS = "contactPermission";
    public static final String SHAREDPREFS_PERMS_STORAGE = "storagePermission";

    public static final String SHAREDPREFS_USER = "user";
    public static final String SHAREDPREFS_WIDGET = "widget";
    public static final String SHAREDPREFS_SYNC = "sync";
    public static final String SHAREDPREFS_WIDGET_REQUEST = "widget_request";

    public static final String API_BASE_URL_PROD = "http://nnjoshi.co/inquest/";
    public static final String API_BASE_URL_DEBUG = "http://nnjoshi.co:6006/inquest/";

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public static void setContactsPermission(Context context, boolean value) {
        SharedPreferences.Editor editor = AppSharedPreferences.editor(context);
        editor.putBoolean(SHAREDPREFS_PERMS_CONTACTS, value);
        editor.commit();
    }

    public static void setStoragePermission(Context context, boolean value) {
        SharedPreferences.Editor editor = AppSharedPreferences.editor(context);
        editor.putBoolean(SHAREDPREFS_PERMS_STORAGE, value);
        editor.commit();
    }

    public static boolean getStoragePermission(Context context) {
        return AppSharedPreferences.getSharedPreferences(context).getBoolean(SHAREDPREFS_PERMS_STORAGE, false);
    }

    public static boolean getContactsPermission(Context context) {
        return AppSharedPreferences.getSharedPreferences(context).getBoolean(SHAREDPREFS_PERMS_CONTACTS, false);
    }

    public static String[] combineStringArrays(String[] first, String[]... rest) {
        int totalLength = first.length;
        for (String[] array : rest) {
            totalLength += array.length;
        }
        String[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (String[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static Bitmap retrieveContactPhoto(Context context, String number) {
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.contact_template);

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
            }

            assert inputStream != null;
            inputStream.close();

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
        return photo;
    }

    public static boolean isFileExtensionAllowed(String inputString, String type)
    {
        String[] items;
        if (type.equals(ResultType.Image.name())) items = ALLOWED_IMAGE_EXTENSIONS;
        else if (type.equals(ResultType.Video.name())) items = ALLOWED_VIDEO_EXTENSIONS;
        else if (type.equals(ResultType.Audio.name())) items = ALLOWED_AUDIO_EXTENSIONS;
        else if (type.equals(ResultType.File.name())) items = ALLOWED_TEXT_EXTENSIONS;
        else items = new String[]{"null"};
        for (String item : items) {
            if (inputString.toLowerCase().endsWith(item.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public static String trimStringToDisplay(String string, String type) {
        if (string.length() < 18) return string;
        else if (type.equals("packageName") && string.length() < 26) return string;
        else if (type.equals("packageName")) {
            StringBuilder trimmed = new StringBuilder();
            trimmed.append(string, 0, 12);
            trimmed.append("...");
            trimmed.append(string, string.length() - 12, string.length());
            return trimmed.toString();
        } else {
            StringBuilder trimmed = new StringBuilder();
            trimmed.append(string, 0, 8);
            trimmed.append("...");
            trimmed.append(string, string.length() - 8, string.length());
            return trimmed.toString();
        }
    }

    public static String getFileExtension(String url) {
        if (url.contains("?")) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.contains("%")) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.contains("/")) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    public static boolean isPro(Context context) {
        return context.getPackageName().equals("co.nnjoshi.android.inquest.pro");
    }

    public static void sync(Context context, boolean showUI) {
        new IndexTask(context, showUI).execute();
    }

    public static int scoreResult(String query, String title, String packageName, String phone) {
        int score = 0;
        if ((title != null) && (title.toLowerCase().contains(query.toLowerCase()))) {
            score += 5;
        }
        if ((packageName != null) && (packageName.toLowerCase().contains(query.toLowerCase()))) {
            score += 2;
        }
        if ((phone != null) && (phone.toLowerCase().contains(query.toLowerCase()))) {
            score += 1;
        }

        return score;
    }

}
