package co.nnjoshi.android.inquest.database;

/**
 * Created by narendranathjoshi on 08/04/16.
 */
public class DatabaseEntry {
    private String _packageName;
    private String _title;
    private String _type;
    private String _image_uri;
    private String _phone;
    private int _score;

    public DatabaseEntry() {
    }

    public DatabaseEntry(String _packageName, String _title, String _type, String _image_uri, String _phone) {
        this._packageName = _packageName;
        this._title = _title;
        this._type = _type;
        this._image_uri = _image_uri;
        this._phone = _phone;
    }

    public String getPackageName() {
        return _packageName;
    }

    public void setPackageName(String _packageName) {
        this._packageName = _packageName;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getType() {
        return _type;
    }

    public void setType(String _type) {
        this._type = _type;
    }

    public String getImageUri() {
        return _image_uri;
    }

    public void setImageUri(String _image_uri) {
        this._image_uri = _image_uri;
    }

    public String getPhone() {
        return _phone;
    }

    public void setPhone(String _phone) {
        this._phone = _phone;
    }

    public int getScore() {
        return _score;
    }

    public void setScore(int _score) {
        this._score = _score;
    }
}
