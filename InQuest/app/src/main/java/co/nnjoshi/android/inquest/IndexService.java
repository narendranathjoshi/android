package co.nnjoshi.android.inquest;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 */
public class IndexService extends Service {
    private boolean isAlreadyRunning;

    @Override
    public void onCreate()
    {
        super.onCreate();
        isAlreadyRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!isAlreadyRunning) {
            isAlreadyRunning = true;
            InquestApplication.sync(IndexService.this, false);
        }
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
