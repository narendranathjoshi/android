package co.nnjoshi.android.inquest.utils;

/**
 * Created by narendranathjoshi on 06/03/16.
 */
public enum ResultType {
    App(0),
//    Widget(1),
    Contact(2),
    Audio(3),
    Video(4),
    File(5),
    Image(6);

    public int index;

    ResultType(int index) {
        this.index = index;
    }
}
